<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Adverts') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/common.css') }}" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-lg ad-navbar">
    <div class="container">
        <a href="{{ route('/') }}" class="navbar-brand ad-navbar-logo"><b>A</b>dverts</a>
        @guest
            <form class="form-inline" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <input class="form-control mr-sm-2 @if ($errors->has('username')) is-invalid @endif" name="username"
                       type="text" placeholder="Enter Username" value="{{ old('username') }}" required>
                <input class="form-control mr-sm-2 @if ($errors->has('password')) is-invalid @endif" name="password"
                       type="password" placeholder="Enter Password" required>
                <button class="btn my-2 my-sm-0" type="submit">Login</button>
            </form>
        @else
            <div class="d-flex justify-content-end">
                <p class="ad-username my-2 mr-sm-2">Welcome, {{ Auth::user()->username }}!</p>
                <a class="btn btn-logout mr-sm-2" href="{{ route('advertCreate') }}">Create Ad</a>
                <a class="btn btn-logout mr-sm-2" href="{{ route('logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
            </div>
        @endguest

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
</nav>

@if ($errors->has('username'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ $errors->first('username') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if ($errors->has('password'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ $errors->first('password') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ $message }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ $message }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@yield('content')

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
</html>
