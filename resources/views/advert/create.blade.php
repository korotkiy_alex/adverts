@extends('layouts.app')

@section('content')
    <main>
        <div class="container">
            <div class="row">
                <div class="card mt-5 mx-auto">
                    <h1 class="card-header">Create advert</h1>
                    <div class="card-body">
                        <form action="{{ route('advertSave') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">

                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control @if ($errors->has('title')) is-invalid @elseif (!is_null(old('title'))) is-valid @endif" id="description" name="title" placeholder="Enter Title" value="{{ old('title') }}">
                                @if ($errors->has('title'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('title') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control @if ($errors->has('description')) is-invalid @elseif (!is_null(old('description'))) is-valid @endif" rows="5" id="description" name="description" placeholder="Enter Description">{{ old('description') }}</textarea>
                                @if ($errors->has('description'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('description') }}
                                    </div>
                                @endif
                            </div>
                            <div class="d-flex justify-content-end">
                                <button type="submit" class="btn">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection