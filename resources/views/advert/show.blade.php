@extends ('layouts.app')

@section('content')
    <main>
        <div class="container">
            <div class="row">
                <div class="card mt-5 mx-auto">
                    <div class="card-header d-flex justify-content-between">
                        <div class="col-9 advert-title">
                            <h5>{{ $advert->title }}</h5>
                        </div>
                        <div class="col-3 text-right advert-info">
                            <p>Author: {{ $advert->user->username }}</p>
                            <p>{{ $advert->created_at }}</p>
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="card-text">{{ $advert->description }}</p>
                        @auth
                            @if (Auth::user()->id == $advert->user_id)
                                <div class="d-flex justify-content-end">
                                    <a href="{{ route('advertEdit', ['id' => $advert->id]) }}"
                                       class="btn mr-sm-2 btn-edit" role="button">Edit</a>
                                    <a href="{{ route('advertDelete', ['id' => $advert->id]) }}"
                                       class="btn mr-sm-2 btn-delete" role="button"
                                       onclick="if (confirm('Are you really want to delete advert?')) {event.preventDefault();document.getElementById('delete-form').submit();} else event.preventDefault()">Delete</a>
                                </div>
                            @endif
                        @endauth
                    </div>
                    <form id="delete-form" action="{{ route('advertDelete', ['id' => $advert->id]) }}" method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection