@extends('layouts.app')

@section('content')
    <main>
        <div class="container">
            <div class="row">
                <div class="card mt-5 mx-auto">
                    <h1 class="card-header">Edit advert {{ $advert->id }}</h1>
                    <div class="card-body">
                        <form action="{{ route('advertUpdate', $advert->id) }}" method="POST">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text"
                                       class="form-control @if ($errors->has('title')) is-invalid @elseif (!is_null(old('title'))) is-valid @endif"
                                       id="description" name="title" placeholder="Enter Title"
                                       value="@if ($errors->has('title') || old('description')){{old('title')}}@else{{$advert->title}}@endif">
                                @if ($errors->has('title'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('title') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea
                                        class="form-control @if ($errors->has('description')) is-invalid @elseif (!is_null(old('description'))) is-valid @endif"
                                        rows="5" id="description" name="description"
                                        placeholder="Enter Description">@if ($errors->has('description') || old('description')){{old('description')}}@else{{$advert->description}}@endif</textarea>
                                @if ($errors->has('description'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('description') }}
                                    </div>
                                @endif
                            </div>
                            <div class="d-flex justify-content-end">
                                <button type="submit" class="btn">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
