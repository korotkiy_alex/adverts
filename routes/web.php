<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', 'AdvertController@index')->name('/');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/edit', 'AdvertController@create')->name('advertCreate');
    Route::get('/edit/{id}', 'AdvertController@edit')->name('advertEdit');
    Route::post('/edit', 'AdvertController@store')->name('advertSave');
    Route::post('/edit/{id}', 'AdvertController@update')->name('advertUpdate');
    Route::post('/delete/{id}', 'AdvertController@delete')->name('advertDelete');

});

Route::get('/{id}', 'AdvertController@show')->name('advertShow');