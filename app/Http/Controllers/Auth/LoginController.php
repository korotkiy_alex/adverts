<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    /**
     * Обработка попытки аутентификации.
     *
     * @return Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        $user = User::select('id')->where('username', $request->username)->first();
        if ($user) {
            $credantials = $request->only('username', 'password');
            if (Auth::attempt($credantials)) {
                return redirect('/')->with('success', 'Successful login!');
            } else {
                throw ValidationException::withMessages([
                    $this->username() => [trans('auth.failed')],
                ]);
            }
        } else {
            $userObj = User::create([
                'username' => $request->username,
                'password' => bcrypt($request->password),
            ]);
            Auth::login($userObj);
            return redirect('/')->with('success', 'Account created successfully!');
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            $this->username().'.max' => 'Max length field username is 32 characters',
            'password.max'  => 'Max length field username is 16 characters',
        ];
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string|max:32',
            'password' => 'required|string|max:16',
        ], $this->messages());
    }
}
