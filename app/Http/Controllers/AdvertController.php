<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advert;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class AdvertController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the list adverts.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adverts = Advert::latest()->paginate(5);
        return view('advert.index', compact('adverts'))->with('i', (request()->input('page', 1) -1)*5);
    }

    /**
     * Show advert.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $advert = Advert::find($id);

        if ($advert) {
            return view('advert.show', compact('advert'));
        }

        return redirect('/')->with('error', 'Not found advert with id ' . $id);
    }

    /**
     * Show the edit advert form.
     *
     * @param  integer $id advert
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advert = Advert::find($id);

        if ($advert) {
            if ($advert->user_id == Auth::user()->id) {
                return view('advert.edit', compact('advert'));
            } else {
                return redirect('/')->with('error', 'You do not have permission to edit this advert');
            }
        }

        return redirect('/')->with('error', 'Not found advert with id ' . $id);
    }

    /**
     * Handle a update advert request to the application.
     *
     * @param  integer $id advert
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateAdvert($request);

        $advert = Advert::find($id);
        $advert->title = $request->get('title');
        $advert->description = $request->get('description');
        $advert->save();
        return redirect()->route('advertShow', $advert->id)->with('success', 'Advert edited successfully!');
    }

    /**
     * Show the create advert form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('advert.create');
    }


    /**
     * Handle a store advert request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validateAdvert($request);
        $advert = Advert::create($request->all());
        return redirect()->route('advertShow', $advert->id)->with('success', 'New advert created successfully!');
    }

    /**
     * Validate the save advert request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateAdvert(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:10000',
        ]);
    }

    /**
     * Handle a update advert request to the application.
     *
     * @param  integer $id advert
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $advert = Advert::find($id);

        if ($advert) {
            $advert->delete();
            return redirect('/')->with('success', 'Advert delete successfully!');
        }

        return redirect('/')->with('error', 'Not found advert with id ' . $id);
    }
}
