Laravel v5.8

Tested with PHP 7, MySQL 5.7

## Instalation

- create .env file from template .env.example
- create database and connect in .env file
- execute command "composer update" in project
- execute command "php artisan key:generate" in project
- execute command "php artisan migrate" in project